variable "resource_group_name" {
  description = "Name of the resource group"
  default     = "Threetierapp16"
}

variable "resource_group_location" {
  description = "Location of the resource group"
  default     = "UK South"
}

variable "client_id" {
  description = "value"
  default = "woP8Q~6_SPKLVnTPjk9oH-OnTp64xZHj8eO1Bb44"

}
# variable "resource_group_config" {
#   description = "Map of resource group names and locations"
#   type        = map(object({
#     name     = string
#     location = string
#   }))
#   default = {
#     "RR_Threetier1" = {
#       name     = "Three-tier-app-1"
#       location = "East US"
#     },
#     "RR_Threetier2" = {
#       name     = "Three-tier-app-2"
#       location = "West US"
#     }
    
#   }
# }
