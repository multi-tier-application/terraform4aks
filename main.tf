provider "azurerm" {
  features {
  }
  client_id       = "85c1194f-a82d-4291-8b12-5e6cc42dbae6"
  client_secret   = var.client_id
  tenant_id       = "eeb85d03-5168-4b27-89fb-df356f62a914"
  subscription_id = "ecb81284-b185-4ee1-aa6d-91e995f46ec7"
}
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.0.0"
    }
  }
  # backend "azurerm" {
  #   resource_group_name   = "Threetierapp" #azurerm_resource_group.RR_Threetier.name
  #   storage_account_name  = "tfstate4aks" #azurerm_storage_account.terraform_storage.name
  #   container_name        = "tfstateforakscluster" # azurerm_storage_container.terraform_container.name
  #   key                   = "terraform.tfstate"
  # }
}

# Create a resource group
resource "azurerm_resource_group" "RRR_Threetier" {
  name     = var.resource_group_name
  location = var.resource_group_location

}
resource "azurerm_storage_account" "terraform_storage1" {
  name                     = "azadsp4tf"   # Choose a unique name
  resource_group_name      = azurerm_resource_group.RRR_Threetier.name
  location                 = azurerm_resource_group.RRR_Threetier.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
  enable_https_traffic_only = true
}

resource "azurerm_storage_container" "terraform_container1" {
  name                  = "azadsp4tf"  # Choose a container name
  storage_account_name  = azurerm_storage_account.terraform_storage1.name
  container_access_type = "private"
}

# create acr Container registery
resource "azurerm_container_registry" "acr1" {
  name                = "azadsp4tf"
  location            = azurerm_resource_group.RRR_Threetier.location
  resource_group_name = azurerm_resource_group.RRR_Threetier.name
  sku                 = "Standard"
  admin_enabled       = true
}
# create AKS Cluster
resource "azurerm_kubernetes_cluster" "aks1" {
  name                = "azadsp4tf-aks1"
  location            = azurerm_resource_group.RRR_Threetier.location
  resource_group_name = azurerm_resource_group.RRR_Threetier.name
  dns_prefix          = "threetierapp"

  default_node_pool {
    name       = "default"
    node_count = 1
    vm_size    = "Standard_D4ps_v5"
  }

  identity {
    type = "SystemAssigned"
  }

  tags = {
    Environment = "stage"
  }
}
# create role assignment for aks acr pull
# resource "azurerm_role_assignment" "example1" {
#   principal_id                     = azurerm_kubernetes_cluster.aks1.kubelet_identity[0].object_id
#   role_definition_name             = "AcrPull"
#   scope                            = azurerm_container_registry.acr1.id
#   skip_service_principal_aad_check = false
# }

# resource "azurerm_resource_group" "RRR_Threetier" {
#   for_each = var.resource_group_config

#   name     = each.value.name
#   location = each.value.location
# }

# # Example of how to access the resource group information
# output "resource_group_info" {
#   value = azurerm_resource_group.RRR_Threetier
# }